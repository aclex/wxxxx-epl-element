/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_SPEED_H
#define EPL_ELEMENT_WIZNET_SPEED_H

namespace epl::element::wiznet
{
	enum class speed : unsigned char
	{
		s10mbps = 0,
		s100mbps = 1
	};
}

#endif // EPL_ELEMENT_WIZNET_SPEED_H
