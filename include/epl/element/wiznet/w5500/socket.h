/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_SOCKET_H
#define EPL_ELEMENT_WIZNET_W5500_SOCKET_H

#include <memory>
#include <span>

#include <epl/net/address.h>

#include <epl/element/wiznet/w5500/protocol.h>
#include <epl/element/wiznet/w5500/command.h>
#include <epl/element/wiznet/w5500/status.h>
#include <epl/element/wiznet/w5500/buffer_size.h>
#include <epl/element/wiznet/w5500/interrupt.h>

namespace epl::element::wiznet::w5500
{
	namespace interrupt::socket
	{
		template<std::size_t> class basic_handler;
	}

	template<class Port, std::size_t N> class socket
	{
	public:
		static bool multicast() noexcept;
		static void set_multicast(const bool st) noexcept;
		static bool broadcast_block() noexcept;
		static void set_broadcast_block(const bool st) noexcept;
		static bool multicast_block() noexcept;
		static void set_multicast_block(const bool st) noexcept;
		static unsigned int igmp_version() noexcept;
		static void set_igmp_version(const unsigned int v) noexcept;
		static bool no_delayed_ack() noexcept;
		static void set_no_delayed_ack(const bool st) noexcept;
		static bool udp_unicast_block() noexcept;
		static void set_udp_unicast_block(const bool st) noexcept;
		static enum protocol protocol() noexcept;
		static void set_protocol(const enum protocol p) noexcept;

		static void open() noexcept;
		static void close() noexcept;

		template<byte_wide T, std::size_t S> static std::size_t send(const std::span<T, S> data) noexcept;
		static std::vector<std::uint8_t> recv(const std::size_t size, const bool no_less = false) noexcept;
		template<byte_wide T, std::size_t S> static std::size_t recv(const std::span<T, S> dest, const bool no_less = false) noexcept;
		static std::size_t available_to_read() noexcept;
		static std::size_t available_to_write() noexcept;

		static enum status status() noexcept;

		static unsigned int source_port() noexcept;
		static void set_source_port(const unsigned int p) noexcept;
		static unsigned int destination_port() noexcept;
		static void set_destination_port(const unsigned int p) noexcept;

		static net::address::ip::v4 destination_address() noexcept;
		static void set_destination_address(net::address::ip::v4& addr) noexcept;
		static net::address::mac destination_hardware_address() noexcept;
		static void set_destination_hardware_address(net::address::mac& addr) noexcept;

		static std::size_t mss() noexcept;
		static void set_mss(const std::size_t v) noexcept;

		static buffer_size rx_buffer_size() noexcept;
		static void set_rx_buffer_size(const buffer_size v) noexcept;
		static buffer_size tx_buffer_size() noexcept;
		static void set_tx_buffer_size(const buffer_size v) noexcept;

		static void enable_interrupt_handling(std::unique_ptr<interrupt::socket::basic_handler<N>>&& h = nullptr) noexcept;
		template<class T, class... Args> static void emplace_interrupt_handling(Args&&... args) noexcept;
		static void disable_interrupt_handling() noexcept;
		static interrupt::socket::flag interrupt() noexcept;
		static void clear_interrupt(const interrupt::socket::flag f) noexcept;
		static void handle_interrupt() noexcept;
		static void wait_for_interrupt(const interrupt::socket::flag f) noexcept;

	protected:
		static void send(const command cmd) noexcept;

	private:
		static std::size_t wait_for_received_size(const std::size_t s, const bool no_less = false) noexcept;

		static std::size_t tx_buffer_free_size() noexcept;
		static std::uint16_t tx_buffer_read_ptr() noexcept;
		static std::uint16_t tx_buffer_write_ptr() noexcept;
		static void set_tx_buffer_write_ptr(const std::uint16_t v) noexcept;

		static std::size_t rx_buffer_received_size() noexcept;
		static std::uint16_t rx_buffer_read_ptr() noexcept;
		static void set_rx_buffer_read_ptr(const std::uint16_t v) noexcept;
		static std::uint16_t rx_buffer_write_ptr() noexcept;

		static constexpr inline std::uint8_t s_multi_mfen_mask{0x80};
		static constexpr inline std::uint8_t s_bcastb_mask{0x40};
		static constexpr inline std::uint8_t s_nd_mc_mmb_mask{0x20};
		static constexpr inline std::uint8_t s_ucastb_mip6b_mask{0x10};

		static constexpr inline std::uint16_t s_mode_address{0x0000};
		static constexpr inline std::uint16_t s_command_address{0x0001};
		static constexpr inline std::uint16_t s_interrupt_address{0x0002};
		static constexpr inline std::uint16_t s_status_address{0x0003};
		static constexpr inline std::uint16_t s_source_port_address{0x0004};
		static constexpr inline std::uint16_t s_destination_hardware_address_address{0x0006};
		static constexpr inline std::uint16_t s_destination_address_address{0x000c};
		static constexpr inline std::uint16_t s_destination_port_address{0x0010};
		static constexpr inline std::uint16_t s_mss_address{0x0012};
		static constexpr inline std::uint16_t s_rx_buffer_size_address{0x001e};
		static constexpr inline std::uint16_t s_tx_buffer_size_address{0x001f};
		static constexpr inline std::uint16_t s_tx_buffer_free_size_address{0x0020};
		static constexpr inline std::uint16_t s_tx_buffer_read_ptr_address{0x0022};
		static constexpr inline std::uint16_t s_tx_buffer_write_ptr_address{0x0024};
		static constexpr inline std::uint16_t s_rx_buffer_received_size_address{0x0026};
		static constexpr inline std::uint16_t s_rx_buffer_read_ptr_address{0x0028};
		static constexpr inline std::uint16_t s_rx_buffer_write_ptr_address{0x002a};
	};

	namespace tcp
	{
		template<class Port, std::size_t N> class socket : public w5500::socket<Port, N>
		{
		public:
			static void open() noexcept;
			static void connect() noexcept;
			static void disconnect() noexcept;
			static void set_protocol(const enum protocol p) noexcept = delete;
		};
	}

	namespace udp
	{
		struct datagram
		{
			struct header_type
			{
				epl::net::address::ip::v4 source_address;
				unsigned int source_port;
				std::size_t size;
			};

			header_type header;
			std::vector<std::uint8_t> payload;
		};

		template<class Port, std::size_t N> class socket : public w5500::socket<Port, N>
		{
		public:
			static void open() noexcept;
			static void set_protocol(const enum protocol p) noexcept = delete;
			static datagram recv() noexcept;
			template<std::integral T, std::size_t C> static std::size_t recv(const std::span<T, C> dest) noexcept;
			static std::size_t available_to_read() noexcept;

		private:
			static datagram::header_type recv_header() noexcept;

			static constexpr std::size_t s_header_size{8};
		};
	}
}

#include "socket.tcc"

#endif // EPL_ELEMENT_WIZNET_W5500_SOCKET_H
