/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_COMMAND_H
#define EPL_ELEMENT_WIZNET_W5500_COMMAND_H

#include <epl/element/wiznet/w5500/interrupt.h>

namespace epl::element::wiznet::w5500
{
	enum class command : unsigned char
	{
		open = 0x01,
		listen = 0x02,
		connect = 0x04,
		discon = 0x08,
		close = 0x10,
		send = 0x20,
		send_mac = 0x21,
		send_keep = 0x22,
		recv = 0x40
	};

	constexpr interrupt::socket::flag success_result(const command cmd) noexcept
	{
		switch (cmd)
		{
		using enum command;
		case send:
			return interrupt::socket::flag::send_ok;

		case connect:
			return interrupt::socket::flag::con;

		case discon:
			return interrupt::socket::flag::discon;

		default:
			return interrupt::socket::flag::no;
		}
	}
}

#endif // EPL_ELEMENT_WIZNET_W5500_COMMAND_H
