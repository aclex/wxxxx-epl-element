/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_INTERRUPT_H
#define EPL_ELEMENT_WIZNET_W5500_INTERRUPT_H

#include <memory>
#include <utility>

namespace epl::element::wiznet::w5500::interrupt
{
	namespace socket
	{
		enum class flag : unsigned char
		{
			no = 0,
			con = 1,
			discon = 2,
			recv = 4,
			timeout = 8,
			send_ok = 16
		};

		constexpr flag operator&(const flag a, const flag b) noexcept
		{
			return flag{static_cast<std::uint8_t>(static_cast<std::uint8_t>(a) & static_cast<std::uint8_t>(b))};
		}

		constexpr flag operator|(const flag a, const flag b) noexcept
		{
			return flag{static_cast<std::uint8_t>(static_cast<std::uint8_t>(a) | static_cast<std::uint8_t>(b))};
		}

		constexpr flag operator~(const flag a) noexcept
		{
			return flag{static_cast<std::uint8_t>(~static_cast<std::uint8_t>(a))};
		}

		constexpr flag operator&=(flag& a, const flag b) noexcept
		{
			a = a & b;
			return a;
		}

		constexpr flag operator|=(flag& a, const flag b) noexcept
		{
			a = a | b;
			return a;
		}

		constexpr bool operator!(const flag a) noexcept
		{
			return !static_cast<bool>(a);
		}

		template<std::size_t N> class basic_handler
		{
		public:
			static basic_handler* instance() {return s_instance.get();}
			virtual void on(const flag f) noexcept
			{
				m_raised_flag |= f;
			}

			flag interrupt() const noexcept {return m_raised_flag;}
			void clear(const flag f) {m_raised_flag &= ~f;}
			virtual ~basic_handler() = default;

		private:
			template<class, std::size_t> friend class w5500::socket;

			static void set_instance(std::unique_ptr<basic_handler>&& h) noexcept
			{
				s_instance = std::move(h);
			}

			template<class T, typename... Args> static void emplace_instance(Args&&... args) noexcept(noexcept(T(std::forward<Args>(args)...)))
			{
				s_instance = std::make_unique<T>(std::forward<Args>(args)...);
			}

			static inline std::unique_ptr<basic_handler> s_instance;
			flag m_raised_flag {flag::no};
		};
	}
}

#endif // EPL_ELEMENT_WIZNET_W5500_INTERRUPT_H
