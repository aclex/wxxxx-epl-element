/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <epl/element/wiznet/w5500/io.h>
#include <epl/element/wiznet/w5500/register_map.h>
#include <epl/element/wiznet/w5500/socket.h>

#include <cassert>

template<class Port> std::vector<std::uint8_t> epl::element::wiznet::w5500::device<Port>::s_data;

template<class Port> void epl::element::wiznet::w5500::device<Port>::reset() noexcept
{
	std::array<std::uint8_t, 1> data{0x80};
	write<Port>(0, register_map::common, std::span{data});

	std::vector<std::uint8_t> received{0};
	while (received[0] & 0x80)
		received = read<Port>(0, register_map::common, 1);
}

template<class Port> void epl::element::wiznet::w5500::device<Port>::refresh() noexcept
{
	s_data = read<Port>(0, register_map::common, s_cr_size);
}

template<class Port> void epl::element::wiznet::w5500::device<Port>::upload() noexcept
{
	write<Port>(0, register_map::common, std::span{s_data});
}

template<class Port> bool epl::element::wiznet::w5500::device<Port>::wake_on_lan() noexcept
{
	return get_bit(&s_data[0], s_wol_mask);
}

template<class Port> void epl::element::wiznet::w5500::device<Port>::set_wake_on_lan(const bool st) noexcept
{
	init_bit(&s_data[0], s_wol_mask, st);
}

template<class Port> bool epl::element::wiznet::w5500::device<Port>::ping_block() noexcept
{
	return get_bit(&s_data[0], s_pb_mask);
}

template<class Port> void epl::element::wiznet::w5500::device<Port>::set_ping_block(const bool st) noexcept
{
	init_bit(&s_data[0], s_pb_mask, st);
}

template<class Port> bool epl::element::wiznet::w5500::device<Port>::pppoe() noexcept
{
	return get_bit(&s_data[0], s_pppoe_mask);
}

template<class Port> void epl::element::wiznet::w5500::device<Port>::set_pppoe(const bool st) noexcept
{
	init_bit(&s_data[0], s_pppoe_mask, st);
}

template<class Port> bool epl::element::wiznet::w5500::device<Port>::force_arp() noexcept
{
	return get_bit(&s_data[0], s_force_arp_mask);
}

template<class Port> void epl::element::wiznet::w5500::device<Port>::set_force_arp(const bool st) noexcept
{
	init_bit(&s_data[0], s_force_arp_mask, st);
}

template<class Port> epl::duplex epl::element::wiznet::w5500::device<Port>::duplex() noexcept
{
	return get_bit(&s_data[46], s_duplex_mask) ? duplex::full : duplex::half;
}

template<class Port> epl::element::wiznet::speed epl::element::wiznet::w5500::device<Port>::speed() noexcept
{
	return get_bit(&s_data[46], s_speed_mask) ? speed::s100mbps : speed::s10mbps;
}

template<class Port> bool epl::element::wiznet::w5500::device<Port>::link() noexcept
{
	return get_bit(&s_data[46], s_link_mask);
}

template<class Port> std::span<std::uint8_t> epl::element::wiznet::w5500::device<Port>::gateway() noexcept
{
	return std::span{std::begin(s_data) + 1, std::begin(s_data) + 5};
}

template<class Port> template<std::integral T, std::size_t N> void epl::element::wiznet::w5500::device<Port>::set_gateway(const std::span<T, N> addr) noexcept
{
	static_assert(sizeof(T) == sizeof(std::uint8_t), "Address should consist of 8-bit elements.");
	assert(addr.size() >= 4);
	std::copy(std::begin(addr), std::begin(addr) + 4, std::begin(s_data) + 1);
}

template<class Port> std::span<std::uint8_t> epl::element::wiznet::w5500::device<Port>::subnet_mask() noexcept
{
	return std::span{std::begin(s_data) + 5, std::begin(s_data) + 9};
}

template<class Port> template<std::integral T, std::size_t N> void epl::element::wiznet::w5500::device<Port>::set_subnet_mask(const std::span<T, N> addr) noexcept
{
	static_assert(sizeof(T) == sizeof(std::uint8_t), "Address should consist of 8-bit elements.");
	assert(addr.size() >= 4);
	std::copy(std::begin(addr), std::begin(addr) + 4, std::begin(s_data) + 5);
}

template<class Port> std::span<std::uint8_t> epl::element::wiznet::w5500::device<Port>::source_hardware_address() noexcept
{
	return std::span{std::begin(s_data) + 9, std::begin(s_data) + 15};
}

template<class Port> template<std::integral T, std::size_t N> void epl::element::wiznet::w5500::device<Port>::set_source_hardware_address(const std::span<T, N> addr) noexcept
{
	static_assert(sizeof(T) == sizeof(std::uint8_t), "Address should consist of 8-bit elements.");
	assert(addr.size() >= 6);
	std::copy(std::begin(addr), std::begin(addr) + 6, std::begin(s_data) + 9);
}

template<class Port> std::span<std::uint8_t> epl::element::wiznet::w5500::device<Port>::source_ip_address() noexcept
{
	return std::span{std::begin(s_data) + 15, std::begin(s_data) + 19};
}

template<class Port> template<std::integral T, std::size_t N> void epl::element::wiznet::w5500::device<Port>::set_source_ip_address(const std::span<T, N> addr) noexcept
{
	static_assert(sizeof(T) == sizeof(std::uint8_t), "Address should consist of 8-bit elements.");
	assert(addr.size() >= 4);
	std::copy(std::begin(addr), std::begin(addr) + 4, std::begin(s_data) + 15);
}

template<class Port> unsigned char epl::element::wiznet::w5500::device<Port>::version() noexcept
{
	return s_data[0x57];
}

template<class Port> template<class T> void epl::element::wiznet::w5500::device<Port>::bind_interrupt() noexcept
{
	class irq_handler : public T::basic_irq_handler_type
	{
		void on_event() noexcept override
		{
			T::irq::clear();
			device::handle_interrupt<T>();
		}
	};
	using led2 = gpio::pin<gpio::port::b, 0>;
	led2::set(epl::state::on);

	T::irq::template emplace_handling<epl::gpio::irq::edge::falling, irq_handler>();
}

template<class Port> template<class T> void epl::element::wiznet::w5500::device<Port>::handle_interrupt() noexcept
{
	using handler_type = void (*)();
	static const std::array<handler_type, socket_count> socket_handlers
	{
		socket<Port, 0>::handle_interrupt,
		socket<Port, 1>::handle_interrupt,
		socket<Port, 2>::handle_interrupt,
		socket<Port, 3>::handle_interrupt,
		socket<Port, 4>::handle_interrupt,
		socket<Port, 5>::handle_interrupt,
		socket<Port, 6>::handle_interrupt,
		socket<Port, 7>::handle_interrupt
	};

	do
	{
		const auto sir{read<Port>(0x17, register_map::common, 1)[0]};

		if (!sir)
			break;

		for (auto i = 0; i < socket_count; ++i)
		{
			if (sir & (1 << i))
			{
				socket_handlers[i]();
			}
		}
	}
	while (!T::irq::pending());
}

template<class Port> template<std::size_t N> void epl::element::wiznet::w5500::device<Port>::enable_socket_interrupt() noexcept
{
	constexpr std::uint8_t mask{1 << N};

	auto& simr{s_data[0x18]};

	simr |= mask;
}

template<class Port> template<std::size_t N> void epl::element::wiznet::w5500::device<Port>::disable_socket_interrupt() noexcept
{
	constexpr std::uint8_t mask{1 << N};

	auto& simr{s_data[0x18]};

	simr &= ~mask;
}
