/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <epl/state.h>

namespace epl::element::wiznet::w5500::detail
{
	template<class Derived, class Port, byte_wide T, std::size_t N, class HeaderType = std::span<std::uint8_t, 3>, class RxStream = epl::dma::stream<epl::spi::dma::slot<Port>, epl::dma::memory::slot<HeaderType>>> class irq_handler : public RxStream::basic_irq_handler_type
	{
		using parent_type = RxStream::basic_irq_handler_type;

	public:
		using rx_dma_stream_type = RxStream;
		using tx_dma_stream_type = epl::dma::stream<epl::dma::memory::slot<HeaderType>, epl::spi::dma::slot<Port>>;
		using port_type = Port;

		explicit irq_handler(const std::span<T, N> data, volatile bool& done) noexcept :
			m_data(data),
			m_state(state::header),
			m_done(done)
		{
			rx_dma_stream_type::configure(m_placeholder.data());
			rx_dma_stream_type::set_increment(epl::state::off);
		}

		void on_event(const dma::irq::event::full_transfer_finish& e) noexcept override
		{
			if (m_state == state::header)
			{
				this->header_to_payload();
			}
			else
			{
				parent_type::on_event(e);
				this->finish();
			}
		}

	protected:
		std::span<T, N> data() const noexcept
		{
			return m_data;
		}

		const std::array<std::uint8_t, 1>& placeholder() const noexcept
		{
			return m_placeholder;
		}

	private:
		enum class state : unsigned char
		{
			header,
			payload
		};

		void header_to_payload() noexcept
		{
			rx_dma_stream_type::disable();
			tx_dma_stream_type::disable();

			m_placeholder.fill(0);

			static_cast<Derived*>(this)->set_addresses();

			rx_dma_stream_type::reload(m_data.size());
			tx_dma_stream_type::reload(m_data.size());

			static_cast<Derived*>(this)->set_increments();

			tx_dma_stream_type::clear_irq();
			rx_dma_stream_type::clear_irq();

			m_state = state::payload;

			rx_dma_stream_type::enable();
			tx_dma_stream_type::enable();
		}

		void finish() noexcept
		{
			port_type::disable();
			tx_dma_stream_type::disable();

			m_done = true;
		}

		std::array<std::uint8_t, 1> m_placeholder;
		const std::span<T, N> m_data;
		state m_state;
		volatile bool& m_done;
	};

	template<direction dir> std::array<std::uint8_t, 3> create_header(const std::uint16_t offset_address, const std::uint8_t register_address) noexcept
	{
		return std::array<std::uint8_t, 3>
		{
			static_cast<std::uint8_t>(offset_address >> 8),
			static_cast<std::uint8_t>(offset_address & 0xff),
			create_control_phase<dir>(register_address)
		};
	}

	template<class IRQHandler, direction dir, byte_wide T, std::size_t N> void op(const std::uint16_t offset_address, const std::uint8_t register_address, const std::span<T, N> data) noexcept
	{
		using namespace std;
		using namespace std::chrono;

		using namespace epl::dma;

		using rx_dma_stream_type = IRQHandler::rx_dma_stream_type;
		using tx_dma_stream_type = IRQHandler::tx_dma_stream_type;
		using port_type = IRQHandler::port_type;

		array<uint8_t, 3> header{create_header<dir>(offset_address, register_address)};

		volatile bool done{};

		while(port_type::busy())
			epl::this_thread::sleep_for(1ms);

		tx_dma_stream_type::configure(header.data());

		rx_dma_stream_type::template emplace_irq_handling<IRQHandler>(data, done);

		constexpr auto cause{rx_dma_stream_type::irq::cause};
		epl::irq::unit::configure(cause, epl::irq::mode::non_vector, 2);

		rx_dma_stream_type::enable();
		tx_dma_stream_type::enable();
		port_type::enable();

		while (!done)
			epl::irq::unit::wait_for_interrupt();
	}
}

template<class Port> std::vector<std::uint8_t> epl::element::wiznet::w5500::read(const std::uint16_t offset_address, const std::uint8_t register_address, const std::size_t N) noexcept
{
	std::vector<std::uint8_t> result(N);

	read<Port>(offset_address, register_address, std::span{result});

	return result;
}

template<class Port, epl::element::wiznet::byte_wide T, std::size_t N> std::size_t epl::element::wiznet::w5500::read(const std::uint16_t offset_address, const std::uint8_t register_address, const std::span<T, N> dest) noexcept
{
	class irq_handler : public detail::irq_handler<irq_handler, Port, T, N>
	{
		using parent_type = detail::irq_handler<irq_handler, Port, T, N>;

	public:
		using parent_type::parent_type;

		void set_addresses() noexcept
		{
			parent_type::rx_dma_stream_type::set_address(this->data().data());
			parent_type::tx_dma_stream_type::set_address(this->placeholder().data());
		}

		static void set_increments() noexcept
		{
			parent_type::rx_dma_stream_type::set_increment(epl::state::on);
			parent_type::tx_dma_stream_type::set_increment(epl::state::off);
		}
	};

	detail::template op<irq_handler, direction::in>(offset_address, register_address, dest);

	return dest.size();
}

template<class Port, epl::element::wiznet::byte_wide T, std::size_t N> void epl::element::wiznet::w5500::write(const std::uint16_t offset_address, const std::uint8_t register_address, const std::span<T, N> data) noexcept
{
	class irq_handler : public detail::irq_handler<irq_handler, Port, T, N>
	{
		using parent_type = detail::irq_handler<irq_handler, Port, T, N>;

	public:
		using parent_type::parent_type;

		void set_addresses() noexcept
		{
			parent_type::rx_dma_stream_type::set_address(this->placeholder().data());
			parent_type::tx_dma_stream_type::set_address(this->data().data());
		}

		static void set_increments() noexcept
		{
			parent_type::rx_dma_stream_type::set_increment(epl::state::off);
			parent_type::tx_dma_stream_type::set_increment(epl::state::on);
		}
	};

	detail::template op<irq_handler, direction::out>(offset_address, register_address, data);
}
