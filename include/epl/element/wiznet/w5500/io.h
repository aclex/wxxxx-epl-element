/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_IO_H
#define EPL_ELEMENT_WIZNET_W5500_IO_H

#include <cstdint>
#include <vector>
#include <span>

#include <epl/direction.h>
#include <epl/dma.h>

#include <epl/element/wiznet/byte_wide.h>

namespace epl::element::wiznet::w5500
{
	template<class Port> std::vector<std::uint8_t> read(const std::uint16_t offset_address, const std::uint8_t register_address, const std::size_t N) noexcept;
	template<class Port, byte_wide T, std::size_t N> std::size_t read(const std::uint16_t offset_address, const std::uint8_t register_address, const std::span<T, N> dest) noexcept;
	template<class Port, byte_wide T, std::size_t N> void write(const std::uint16_t offset_address, const std::uint8_t register_address, const std::span<T, N> data) noexcept;

	namespace detail
	{
		template<direction d> std::uint8_t create_control_phase(const std::uint8_t register_address)
		{
			constexpr std::uint8_t mode_bit{d == direction::out ? 1 : 0};
			return (register_address << 3) | (mode_bit << 2); // we always use VDM mode
		}
	}
}

#include "io.tcc"

#endif // EPL_ELEMENT_WIZNET_W5500_IO_H
