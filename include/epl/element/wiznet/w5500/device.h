/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_DEVICE_H
#define EPL_ELEMENT_WIZNET_W5500_DEVICE_H

#include <array>
#include <vector>

#include <epl/state.h>
#include <epl/duplex.h>
#include <epl/net/address.h>

#include <epl/element/wiznet/speed.h>

namespace epl::element::wiznet::w5500
{
	template<class Port, std::size_t N> class socket;

	template<class Port> class device
	{
	public:
		static constexpr inline std::size_t socket_count{8};

		static void reset() noexcept;
		static void refresh() noexcept;
		static void upload() noexcept;

		static bool wake_on_lan() noexcept;
		static void set_wake_on_lan(const bool st) noexcept;
		static bool ping_block() noexcept;
		static void set_ping_block(const bool st) noexcept;
		static bool pppoe() noexcept;
		static void set_pppoe(const bool st) noexcept;
		static bool force_arp() noexcept;
		static void set_force_arp(const bool st) noexcept;
		static enum speed speed() noexcept;
		static epl::duplex duplex() noexcept;
		static bool link() noexcept;

		static std::span<std::uint8_t> gateway() noexcept;
		template<std::integral T, std::size_t N> static void set_gateway(const std::span<T, N> addr) noexcept;
		static std::span<std::uint8_t> subnet_mask() noexcept;
		template<std::integral T, std::size_t N> static void set_subnet_mask(const std::span<T, N> addr) noexcept;
		static std::span<std::uint8_t> source_hardware_address() noexcept;
		template<std::integral T, std::size_t N> static void set_source_hardware_address(const std::span<T, N> addr) noexcept;
		static std::span<std::uint8_t> source_ip_address() noexcept;
		template<std::integral T, std::size_t N> static void set_source_ip_address(const std::span<T, N> addr) noexcept;

		static unsigned char version() noexcept;

		template<class T> static inline void bind_interrupt() noexcept;
		template<class T> static inline void handle_interrupt() noexcept;

	private:
		template<class, std::size_t> friend class epl::element::wiznet::w5500::socket;

		template<std::size_t N> static inline void enable_socket_interrupt() noexcept;
		template<std::size_t N> static inline void disable_socket_interrupt() noexcept;

		static constexpr inline std::size_t s_cr_size{58};

		static constexpr inline std::uint8_t s_wol_mask{0x20};
		static constexpr inline std::uint8_t s_pb_mask{0x10};
		static constexpr inline std::uint8_t s_pppoe_mask{0x04};
		static constexpr inline std::uint8_t s_force_arp_mask{0x02};

		static constexpr inline std::uint8_t s_duplex_mask{0x04};
		static constexpr inline std::uint8_t s_speed_mask{0x02};
		static constexpr inline std::uint8_t s_link_mask{0x01};

		static std::vector<std::uint8_t> s_data;
	};
}

#include "device.tcc"

#endif // EPL_ELEMENT_WIZNET_W5500_DEVICE_H
