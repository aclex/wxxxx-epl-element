/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <array>
#include <span>

#include <epl/thread.h>
#include <epl/spi.h>
#include <epl/irq.h>

#include <epl/element/wiznet/w5500/io.h>
#include <epl/element/wiznet/w5500/register_map.h>

using namespace std;

using namespace epl;
using namespace epl::spi;

using namespace epl::element::wiznet::w5500;

namespace
{
	using miso = gpio::pin<gpio::port::a, 6>;
	using mosi = gpio::pin<gpio::port::a, 7>;
	using sck = gpio::pin<gpio::port::a, 5>;
	using nss = gpio::pin<gpio::port::a, 4>;

	using spi0 = spi::port<0, sck, miso, mosi, nss>;
}

int main(int, char**)
{
	epl::irq::unit::reset();
	epl::irq::unit::enable();

	spi0::configure(spi::mode::master, duplex::full, spi::word_length::b8, spi::prescaler::p256, spi::bit_order::msb, spi::nss_mode::output);

	vector<uint8_t> test(19, 0x00);

	while(true)
	{
		auto s{read<spi0>(0, register_map::common, 64)};
		this_thread::sleep_for(500ms);
		write<spi0>(0, register_map::common, span<uint8_t>{test});
		this_thread::sleep_for(500ms);
	}
}
