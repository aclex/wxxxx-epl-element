/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <array>
#include <span>

#include <epl/thread.h>
#include <epl/spi.h>
#include <epl/irq.h>

#include <epl/element/wiznet/w5500/device.h>
#include <epl/element/wiznet/w5500/socket.h>

using namespace std;

using namespace epl;
using namespace epl::spi;

using namespace epl::element::wiznet::w5500;

namespace
{
	using miso = gpio::pin<gpio::port::a, 6>;
	using mosi = gpio::pin<gpio::port::a, 7>;
	using sck = gpio::pin<gpio::port::a, 5>;
	using nss = gpio::pin<gpio::port::a, 4>;
	using irt = gpio::pin<gpio::port::a, 2>;

	using spi0 = spi::port<0, sck, miso, mosi, nss>;
	using my_device = device<spi0>;
	using used_socket = socket<spi0, 0>;
	using tcp_socket = tcp::socket<spi0, 1>;

	class my_socket : public used_socket
	{
	public:
		template<size_t Size> static size_t recv(const span<uint8_t, Size> buffer) noexcept
		{
			static constexpr size_t w5500_header_size{8};

			auto packet{used_socket::recv(buffer.size())};

			if (packet.size() < w5500_header_size)
			{
				return 0;
			}

			const auto result{min(packet.size() - w5500_header_size, buffer.size())};
			copy(begin(packet) + w5500_header_size, begin(packet) + result, begin(buffer));

			return result;
		}
	};

	// class dhcp_options
	// {
	// public:
		// template<size_t N> explicit dhcp_options(const span<uint8_t, N> memory) noexcept :
			// m_data(memory)
		// {
			// parse();
		// }

	// private:
		// void parse()
		// {

		// }

		// const span<uint8_t, dynamic_extent> m_data;
		// unordered_map<uint8_t, vector<span<uint8_t, dynamic_extent>> m_option_bookmarks;
	// };

	template<bool buffer> struct dhcp_buffer
	{
		static constexpr size_t max_length{576};
	};

	template<> struct dhcp_buffer<true> : dhcp_buffer<false>
	{
		vector<uint8_t> m_buffer = vector<uint8_t>(dhcp_buffer<false>::max_length, 0);
	};

	template<bool buffer = true> struct dhcp_message : /*protected*/ dhcp_buffer<buffer>
	{
	// private:
		const span<uint8_t, dhcp_buffer<buffer>::max_length> m_data;

	public:
		dhcp_message() noexcept :
			dhcp_message(span<uint8_t>{})
		{
			initialize();
		}

		template<size_t N> dhcp_message(const span<uint8_t, N> data) noexcept :
			m_data(buffer ? span{dhcp_buffer<buffer>::m_buffer} : data),
			opcode(m_data.data()),
			htype(m_data.data() + 1),
			hlen(m_data.data() + 2),
			hops(m_data.data() + 3),
			xid(m_data.data() + 4),
			secs(m_data.data() + 8),
			flags(m_data.data() + 10),
			ciaddr(m_data.data() + 12),
			yiaddr(m_data.data() + 16),
			siaddr(m_data.data() + 20),
			giaddr(m_data.data() + 24),
			chaddr(m_data.data() + 28),
			sname(m_data.data() + 44),
			file(m_data.data() + 108),
			magic_cookie(m_data.data() + 236),
			options(m_data.data() + 240), // 'magic cookie' excluded
			endmark(m_data.data() + 575),
			end(m_data.data() + 576)
		{

		}

		void initialize() noexcept
		{
			xid[0] = 0xba;
			xid[1] = 0x18;
			xid[2] = 0x32;
			xid[3] = 0x0f;

			magic_cookie[0] = 0x63;
			magic_cookie[1] = 0x82;
			magic_cookie[2] = 0x53;
			magic_cookie[3] = 0x63;

			*endmark = 0xff;
		}

		span<uint8_t> data() const noexcept
		{
			return m_data.subspan(0, size());
		}

		size_t size() const noexcept
		{
			return end - opcode;
		}

		void resize(const size_t s) noexcept
		{
			end = opcode + s;
		}

		void close(uint8_t* const after_data) noexcept
		{
			endmark = after_data;
			*endmark = 0xff;
			end = endmark + 1;
		}

		uint8_t* const opcode;
		uint8_t* const htype;
		uint8_t* const hlen;
		uint8_t* const hops;
		uint8_t* const xid;
		uint8_t* const secs;
		uint8_t* const flags;
		uint8_t* const ciaddr;
		uint8_t* const yiaddr;
		uint8_t* const siaddr;
		uint8_t* const giaddr;
		uint8_t* const chaddr;
		uint8_t* const sname;
		uint8_t* const file;
		uint8_t* const options;
		uint8_t* endmark;
		uint8_t* end;

	private:
		uint8_t* const magic_cookie;
	};

	using led = gpio::pin<gpio::port::b, 0>;
}

int main(int, char**)
{
	epl::irq::unit::reset();
	epl::irq::unit::set_level_bits(3);

	epl::irq::unit::enable();

	led::configure(gpio::output_mode::push_pull); // or gpio::configure<led>(...)

	led::set(epl::state::off);

	spi0::configure(spi::mode::master, duplex::full, spi::word_length::b8, spi::prescaler::p256, spi::bit_order::msb, spi::nss_mode::output);

	irt::configure(gpio::mode::input, gpio::pull::up);

	my_device::template bind_interrupt<irt>();

	net::address::ip::v4 bb_addr{255, 255, 255, 255};
	net::address::ip::v4 subnet_mask{255, 255, 255, 0};
	net::address::ip::v4 gateway_addr{192, 168, 1, 1};

	net::address::mac hw_addr{'t', 'e', 's', 't', 'm', 'e'}; // 74:65:73:74:6d:65
	net::address::mac bb_hw_addr{0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

	my_device::reset();

	// this_thread::sleep_for(5s);

	my_device::refresh();
	my_device::set_source_hardware_address(span{hw_addr});
	my_socket::enable_interrupt_handling();
	my_device::upload();

	my_socket::close();
	my_socket::set_protocol(protocol::udp);
	my_socket::set_source_port(68);
	my_socket::set_destination_port(67);
	my_socket::set_destination_address(bb_addr);
	my_socket::set_destination_hardware_address(bb_hw_addr);
	my_socket::set_tx_buffer_size(buffer_size::s2kb);
	my_socket::set_rx_buffer_size(buffer_size::s2kb);

	my_socket::open();

	dhcp_message msg;

	// sending DISCOVER
	msg.opcode[0] = 1;
	msg.htype[0] = 1;
	msg.hlen[0] = 6;
	copy(begin(hw_addr), end(hw_addr), msg.chaddr);

	// TODO: options
	msg.options[0] = 53;
	msg.options[1] = 1;
	msg.options[2] = 1;
	msg.close(msg.options + 3);

	my_socket::send(msg.data());

	// receiving OFFER
	msg.resize(msg.max_length);
	auto read_count{my_socket::recv(msg.data())};
	msg.resize(read_count);

	// sending REQUEST
	my_socket::set_destination_address(bb_addr);
	my_socket::set_destination_hardware_address(bb_hw_addr);

	msg.opcode[0] = 1;
	copy(msg.yiaddr, msg.yiaddr + 4, msg.ciaddr);
	fill(msg.yiaddr, msg.yiaddr + 4, 0);
	fill(msg.options, msg.options + 314, 0);

	// // TODO: options
	msg.options[0] = 53;
	msg.options[1] = 1;
	msg.options[2] = 3;
	msg.options[3] = 50;
	msg.options[4] = 4;
	copy(msg.ciaddr, msg.ciaddr + 4, msg.options + 5);
	msg.options[9] = 54;
	msg.options[10] = 4;
	copy(msg.siaddr, msg.siaddr + 4, msg.options + 11);
	msg.options[15] = 61;
	msg.options[16] = 6;
	copy(begin(hw_addr), end(hw_addr), msg.options + 17);
	msg.options[23] = 51;
	msg.options[24] = 4;
	msg.options[25] = 0;
	msg.options[26] = 0;
	msg.options[27] = 0x02;
	msg.options[28] = 0x58;
	msg.close(msg.options + 29);

	my_socket::send(msg.data());

	// receiving ACK
	read_count = my_socket::recv(msg.data());
	msg.resize(read_count);

	// TODO: extract options
	net::address::ip::v4 my_ip_addr;
	copy(msg.yiaddr, msg.yiaddr + 4, begin(my_ip_addr));

	read_count = my_socket::recv(msg.data());

	my_device::set_source_ip_address(span{my_ip_addr});
	my_device::set_subnet_mask(span{subnet_mask});
	my_device::set_gateway(span{gateway_addr});
	my_device::upload();

	this_thread::sleep_for(1s);

	// now TCP client

	my_socket::close();

	net::address::ip::v4 server_addr{192, 168, 1, 2};
	tcp_socket::set_source_port(1026);
	tcp_socket::set_destination_port(3342);
	tcp_socket::enable_interrupt_handling();
	tcp_socket::set_destination_address(server_addr);
	my_device::upload();

	tcp_socket::open();

	tcp_socket::connect();

	vector<uint8_t> pkt{'{', '\"', 'm', 's', 'g', '\"', ':', '\"', 'H', 'e', 'l', 'l', 'o', ' ', 'f', 'r', 'o', 'm', ' ', 'R', 'I', 'S', 'C', '-', 'V', '!', '\"', ',', ' ', '\"', 's', 'o', 'u', 'r', 'c', 'e', '\"', ':', '\"', 'm', 'c', 'u', '\"', '}'};
	tcp_socket::send(span{pkt});

	vector<uint8_t> received(512, 0);

	read_count = tcp_socket::recv(span{received});

	tcp_socket::disconnect();
	tcp_socket::close();

	led::set(epl::state::on);

	while(true)
	{
		epl::irq::unit::wait_for_interrupt();
	}
}
